<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));
        $this->load->library('session');
        $this->load->model('usuarios_model', 'usuarios');
    }

    public function index()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('email', 'E-mail', 'trim|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');

        if ($this->form_validation->run() == false) {
            $this->load->view('login');
        } else {

            $valid = $this->usuarios->validate();
            if ($valid) {
                $data = array(
                    'logged' => true,
                );
                $this->session->set_userdata($data);
                redirect('home');
            } else {
                $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">E-mail ou senha incorretos!</div>');
                redirect('login');
            }
        }

    }    
}
