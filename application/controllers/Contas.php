<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contas extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('grocery_CRUD');
    }

    public function index()
    {

        $crud = new grocery_CRUD();
        $crud->set_table('contas');
        $crud->required_fields('fornecedor_id','codigo', 'agencia', 'conta_corrente');
        $crud->set_relation('fornecedor_id','fornecedores','razao_social');
        $crud->display_as('fornecedor_id','Fornecedor');
        $crud->field_type('codigo', 'dropdown', array(
            '001' => 'BANCO DO BRASIL S.A (BB)',
            '237' => 'BRADESCO S.A',
            '335' => 'Banco Digio S.A',        
        ));

        $crud->unset_clone();

        $output = $crud->render();

        $this->template('contas.php', $output);
    }
   
}
