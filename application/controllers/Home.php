<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));
        if ($this->session->userdata('logged') == false) {
            redirect('login');
        }
    }

    public function index()
    {
        $this->template('home');
    }   

    public function logout()
    {
        $data = array(
            'logged' => false,
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect('home/index');
    }
}
