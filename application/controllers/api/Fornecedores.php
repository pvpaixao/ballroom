<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

require APPPATH . 'libraries/REST_Controller.php';

class Fornecedores extends REST_Controller
{

    public function __construct()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        parent::__construct();
        $this->load->model('fornecedores_model', 'fornecedores');

    }

    public function index_get()
    {
        $data = $this->fornecedores->lists();
        $this->response($data, 200);
    }

    public function index_post()
    {
        $result = $this->fornecedores->create();

        if ($result === false) {
            $this->response(array('status' => 'error'));
        } else {
            $this->response(array('status' => 'success'));
        }
    }
}
