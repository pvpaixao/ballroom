<?php
class Usuarios_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function validate()
    {

        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('email', $this->input->post('email'));
        $query = $this->db->get();
        if ($query->num_rows() == 1) {
            $row = $query->row();
            if (password_verify($this->input->post('password'), $row->password)) {
                return $row;
            } else {
                return false;
            }
        }

        return false;
    }
}
