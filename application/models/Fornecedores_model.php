<?php
class Fornecedores_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function lists()
    {

        $this->db->select('*');
        $this->db->from('fornecedores');
        //$this->db->join('contas', 'contas.fornecedor_id = fornecedores.id');
        return $this->db->get()->result_array();
    }

    public function getbyid($id)
    {
        $this->db->select('*');
        $this->db->from('fornecedores');
        $this->db->where("id", $id);
        $data = $this->db->get()->row();
    }

    public function create()
    {
        $data = array(
            'razao_social' => $this->input->post('razao_social'),
            'cnpj' => $this->input->post('cnpj'),
            'email' => $this->input->post('email'),
            'telefone' => $this->input->post('telefone'),
            'cidade' => $this->input->post('cidade'),
            'estado' => $this->input->post('estado'),
        );
        return $this->db->insert('fornecedores', $data);
    }
}
