  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url();?>home" class="brand-link">
      <span class="brand-text font-weight-light">Ballroom</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">   
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->         
          <li class="nav-item">
            <a href="<?php base_url()?>/fornecedores" class="nav-link">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Fornecedores
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php base_url()?>/contas" class="nav-link">
              <i class="nav-icon fa fa-credit-card"></i>
              <p>
                Contas
              </p>
            </a>
          </li>        
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>