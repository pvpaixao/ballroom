<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function pr($a)
{
    echo '<pre>', print_r($a, 1), '</pre>';
}

class MY_Controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url'));

    }
  
    function template($content, $data = [])
    {
        $this->load->view('header', $data);
        $this->load->view('sidebar');
        $this->load->view($content, $data); 
        $this->load->view('footer');
    }
   
}
