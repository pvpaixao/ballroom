FROM php:7.4-apache

ENV APACHE_DOCUMENT_ROOT /var/www/html
ENV APACHE_LOG_DIR /var/log/apache2

RUN apt-get update && apt-get install git libzip-dev vim  -y

RUN docker-php-ext-install zip mysqli pdo pdo_mysql  

RUN a2enmod rewrite

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# seta o document root configurado anteriormente nas variáveis de ambiente
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf

COPY . /var/www/html
WORKDIR /var/www/html
RUN ls

RUN chown -R www-data:www-data /var/www/html
RUN chmod +x ./entrypoint

CMD ["./entrypoint"]
